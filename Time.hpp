//  ForBy LTD 2017
/// Time Tools

#ifndef FORBY_UTILS_TIME_INCLUDED
#define FORBY_UTILS_TIME_INCLUDED

#define _forby_utils_time_edition 0x0001

#include <sstream>
#include <ctime>

#include <ForBy/Utils/StringTools.hpp>

namespace ForBy{
namespace Utils
{

inline std::string
get_date()
{
  std::stringstream Date;
  static time_t t;
  static struct tm* lt;

  t = time(0);
  lt = localtime(&t);

  Date << "[ " << lt->tm_hour
       << ":"  << lt->tm_min
       << "."  << lt->tm_sec
       << " (" << lt->tm_mday
       << "/"  << lt->tm_mon
       << "/"  << (lt->tm_year+1900)
       << ") ] ";

  return Date.str();
}



} // namespace Utils
} // namespace ForBy


#endif // FORBY_UTILS_TIME_INCLUDED
